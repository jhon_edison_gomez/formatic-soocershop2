<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Clubs_bl {

    public static function create($data) {
        $uri = G_PUBLIC."shields/";
        $image = Fox\Utils\Image::upload($uri, $_FILES["shield"],$data["name"]);
        $data["shield"] = "shields/".$image;
        $club = Club::instanciate($data);
        $r = $club->create();
        return $r;
    }

}
