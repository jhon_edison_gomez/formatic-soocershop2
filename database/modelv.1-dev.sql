-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema soccer_shop
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `soccer_shop` ;

-- -----------------------------------------------------
-- Schema soccer_shop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `soccer_shop` DEFAULT CHARACTER SET utf8 ;
USE `soccer_shop` ;

-- -----------------------------------------------------
-- Table `soccer_shop`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `soccer_shop`.`User` ;

CREATE TABLE IF NOT EXISTS `soccer_shop`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `soccer_shop`.`Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `soccer_shop`.`Country` ;

CREATE TABLE IF NOT EXISTS `soccer_shop`.`Country` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `soccer_shop`.`Club`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `soccer_shop`.`Club` ;

CREATE TABLE IF NOT EXISTS `soccer_shop`.`Club` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `shield` VARCHAR(128) NOT NULL,
  `user` INT NOT NULL,
  `country` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Club_User_idx` (`user` ASC),
  INDEX `fk_Club_Country1_idx` (`country` ASC),
  CONSTRAINT `fk_Club_User`
    FOREIGN KEY (`user`)
    REFERENCES `soccer_shop`.`User` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Club_Country1`
    FOREIGN KEY (`country`)
    REFERENCES `soccer_shop`.`Country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `soccer_shop`.`Position`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `soccer_shop`.`Position` ;

CREATE TABLE IF NOT EXISTS `soccer_shop`.`Position` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `soccer_shop`.`Player`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `soccer_shop`.`Player` ;

CREATE TABLE IF NOT EXISTS `soccer_shop`.`Player` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `country` INT NOT NULL,
  `position` INT NOT NULL,
  `born` VARCHAR(45) NOT NULL,
  `leg` VARCHAR(45) NOT NULL,
  `pac` VARCHAR(45) NOT NULL,
  `sho` VARCHAR(45) NOT NULL,
  `pas` VARCHAR(45) NOT NULL,
  `dri` VARCHAR(45) NOT NULL,
  `def` VARCHAR(45) NOT NULL,
  `phy` VARCHAR(45) NOT NULL,
  `pic` VARCHAR(45) NOT NULL,
  `price` INT NOT NULL,
  `club` INT NOT NULL,
  PRIMARY KEY (`id`, `club`),
  INDEX `fk_Player_Country1_idx` (`country` ASC),
  INDEX `fk_Player_Position1_idx` (`position` ASC),
  INDEX `fk_Player_Club1_idx` (`club` ASC),
  CONSTRAINT `fk_Player_Country1`
    FOREIGN KEY (`country`)
    REFERENCES `soccer_shop`.`Country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Player_Position1`
    FOREIGN KEY (`position`)
    REFERENCES `soccer_shop`.`Position` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Player_Club1`
    FOREIGN KEY (`club`)
    REFERENCES `soccer_shop`.`Club` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `soccer_shop`.`ShoppingCar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `soccer_shop`.`ShoppingCar` ;

CREATE TABLE IF NOT EXISTS `soccer_shop`.`ShoppingCar` (
  `Club_id` INT NOT NULL,
  `Player_id` INT NOT NULL,
  PRIMARY KEY (`Club_id`, `Player_id`),
  INDEX `fk_Club_has_Player1_Player1_idx` (`Player_id` ASC),
  INDEX `fk_Club_has_Player1_Club1_idx` (`Club_id` ASC),
  CONSTRAINT `fk_Club_has_Player1_Club1`
    FOREIGN KEY (`Club_id`)
    REFERENCES `soccer_shop`.`Club` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Club_has_Player1_Player1`
    FOREIGN KEY (`Player_id`)
    REFERENCES `soccer_shop`.`Player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `soccer_shop`.`Country`
-- -----------------------------------------------------
START TRANSACTION;
USE `soccer_shop`;
INSERT INTO `soccer_shop`.`Country` (`id`, `name`) VALUES (1, 'Colombia');
INSERT INTO `soccer_shop`.`Country` (`id`, `name`) VALUES (2, 'Portugal');
INSERT INTO `soccer_shop`.`Country` (`id`, `name`) VALUES (3, 'Argentina');
INSERT INTO `soccer_shop`.`Country` (`id`, `name`) VALUES (4, 'España');
INSERT INTO `soccer_shop`.`Country` (`id`, `name`) VALUES (5, 'Brasil');
INSERT INTO `soccer_shop`.`Country` (`id`, `name`) VALUES (6, 'Belgica');

COMMIT;


-- -----------------------------------------------------
-- Data for table `soccer_shop`.`Position`
-- -----------------------------------------------------
START TRANSACTION;
USE `soccer_shop`;
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (1, 'GK');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (2, 'CB');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (3, 'LB');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (4, 'RB');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (5, 'CDM');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (6, 'CAM');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (7, 'LM');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (8, 'RM');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (9, 'LW');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (10, 'RW');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (11, 'CF');
INSERT INTO `soccer_shop`.`Position` (`id`, `name`) VALUES (12, 'ST');

COMMIT;

