<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Index_controller extends \Fox\FoxController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
      $userId = Fox\Core\Session::get("uid");
      if(empty($userId)){
          header("Location:".URL."Login");
      }
      $this->view->user = User::getById($userId);
      $this->view->club = Users_bl::iHaveAClub($userId);
      $this->view->title="Fox Admin Panel";
      $this->view->render($this,"index");
    }
    
    public function sample()
    {
        $this->view->title="Fox Admin Panel";
        $this->view->render($this,"asyncFragment");
    }
    
    public function testBelongsTo(){
        
        $p = new Player(null, "Falcao", 1, null, "na", "R", 0, 0, 0, 0, 0, 0, 0, 0, 5);
        //var_dump($p);
        $position = Position::getById(2);
        //var_dump($position);
        
        $p->belongsTo("Position", $position);
        //print_r($p);
        
        $r = $p->create();
        print_r($r);
    }
    
    public function testPopulate(){
        
        $falcao = Player::getBy("name", "Falcao");
        //$falcao->populate("from","Position");
        //print_r($falcao);
        //$falcao->setLeg("L");
        //$r = $falcao->update();
        //print_r($r);
    }

}
