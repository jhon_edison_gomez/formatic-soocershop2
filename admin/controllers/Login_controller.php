<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Login_controller extends \Fox\FoxController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $userId = Fox\Core\Session::get("uid");
        if(!empty($userId)){
            header("Location:".URL);
        }
        $this->view->title="Fox Admin Panel | Login";
        $this->view->render($this,"index");
    }
    
    public function login()
    {
        $data = filter_input_array(INPUT_POST);
        $r = Users_bl::login($data);
        \Fox\Core\Penelope::printJSON($r);
    }
    
    public function logout()
    {
        Users_bl::logout();
        header("Location:".URL."Login");
    }

}
