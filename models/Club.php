<?php

/**
* Class Club generated by Chrysalis v.1.0.0
* 
* Chrysalis info:
* @author pabhoz
* github: @pabhoz
* bitbucket: @pabhoz
* 
*/ 
 
class Club extends \Fox\FoxModel {

    private $id;
    private $name;
    private $shield;
    private $user;
    private $country;

    public function __construct( $id,  string $name, string $shield, int $user, int $country) {

        parent::__construct();
        $this->id = $id;
        $this->name = $name;
        $this->shield = $shield;
        $this->user = $user;
        $this->country = $country;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getShield(): string {
        return $this->shield;
    }

    public function getUser(): int {
        return $this->user;
    }

    public function getCountry(): int {
        return $this->country;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function setName(string $name) {
        $this->name = $name;
    }

    public function setShield(string $shield) {
        $this->shield = $shield;
    }

    public function setUser(int $user) {
        $this->user = $user;
    }

    public function setCountry(int $country) {
        $this->country = $country;
    }

    public function getMyVars(){
 return get_object_vars($this);
    }

}